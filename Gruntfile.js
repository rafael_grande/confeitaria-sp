module.exports = function(grunt) {
	grunt.initConfig({
		copy: {
			project: {
				expand: true,
				cwd:'src',
				src:['**', '!public/sass/*.scss', '!public/sass'],
				dest: 'dist'
			}
		},
		clean: {
			dist: {
				src: 'dist'
			}
		},


        compass: {
            dist: {
                options: {
                    outputStyle: 'compressed',
                    config: 'config/compass.rb', 
                }
            }
        },

		connect: {
    		server: {
      			options: {
                    host: '192.168.0.123',
                    port: '9000',
        			keepalive: true,
      			}
    		}	
  		},
        
        jade: {
            compile: {
                options: {
                    pretty: true
                },
                files: [{
                    expand: true, // setting to true enables the following options
                    cwd: 'jade', // src matches are relative to this path
                    src: ['*.jade'], // matches *.jade in cwd and 1 level down
                    dest: '', // destination prefix
                    ext: '.html' // replace existing extensions with this value
                }]
            }
        },
        
        watch: {
            sass: {
                files: 'sass/*.scss',
                tasks: ['compass'],
                options: {
                    event: ['added', 'deleted', 'changed'],
                },
            },
            jade: {
                files: 'jade/*.jade',
                tasks: ['jade'],
                options: {
                  event: ['added', 'deleted', 'changed'],
                },
            },
        
        }

	});
    
    

	grunt.registerTask('default',['clean', 'copy', 'compass', 'connect']);
	grunt.loadNpmTasks('grunt-contrib-copy');
	grunt.loadNpmTasks('grunt-contrib-clean');
	grunt.loadNpmTasks('grunt-contrib-compass');
    grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-connect');
    grunt.loadNpmTasks('grunt-contrib-jade');
};
