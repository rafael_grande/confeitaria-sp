$( document ).ready(function() {
    $('.menu').click(function(){
        $('.op-menu').fadeToggle('slow');

    });

    $('.open-search').click(function(){
        $('.open-search').hide();
        $('.search form').fadeToggle('slow');
    });

    $('footer .title').click(function(){
        if($(this).attr('class').indexOf('open') < 0){
            $(this).addClass('open');
            $(this).parent().parent().children('.accordion').show('slow');
            $(this).children('i').removeClass();
            $(this).children('i').addClass('fa fa-chevron-up');
        } else {
            $(this).removeClass('open');
            $(this).parent().parent().children('.accordion').hide('slow');
            $(this).children('i').removeClass();
            $(this).children('i').addClass('fa fa-chevron-right');
        }
    });
});
