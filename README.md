# Confeitaria São Paulo

### Criar conta no bitbucket e chave ssh:

<https://bitbucket.org/>

<https://confluence.atlassian.com/pages/viewpage.action?pageId=270827678>

## Para começar a trabalhar nesse projeto, siga esses passos:

###Vai no terminal e crie uma pasta no diretorio que quiser:
```
#!teminal
  $ mkdir confeitaria-sp
  $ cd confeitaria-sp

```

### Clonar o projeto para essa pasta:
```
#!teminal
  $ git clone git@bitbucket.org:MJHouse/confeitaria-sp.git
```


### Instale os plugins node
```
#!teminal
  $ npm install
```

### Crie sua branch, pegando as tarefas no trello:
```
#!teminal
  $ git checkout -b nome-branch
```

### Rode o grunt-watch no terminal:
```
#!teminal
  $ grunt-watch
```

### Adiciona suas alteraçoes no git e da o *commit*: 
```
#!teminal
  $ git add .
  $ git commit -m "Descreve suas alteracoes"
```

### E sobe no servidor na sua branch:
```
#!teminal
  $ git push orgin nome-branch
```